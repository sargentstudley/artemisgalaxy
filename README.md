# README #

Artemis Galaxy - a campaign manager for Artemis Space Ship Bridge Simulator. 

### What is this repository for? ###

* Artemis Galaxy is a campaign manager for Artemis Space Ship Bridge Simulator. It takes several scripted mission files and links them together to create a larger campaign, where the actions in one area (mission) have consequences in others. For example, you can upgrade a station in sector 1-1, but if you do so, you can't afford to rebuild the cruiser that was recently lost in battle. 
* 0.0.1 - Unreleased, not even Alpha yet!

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
-Python 3.2+
-Django 1.7
-PostgreSQL for web deployment. 
-Bootstrap
-JQuery

* Database configuration
Thinking PostgreSQL, but not sure yet. 

* How to run tests
TBD.
* Deployment instructions
TBD.

### Contribution guidelines ###
*Models
This repository is following the 3-tiered business application structure. Please do NOT put logic in models. Use any of the packages available, or build your own if you really need to. 
Packages should exist on the same level as the templates folder.

* Writing tests
Please test code before pushing. Unit tests against models and game logic are highly encouraged.

* Branching Strategy
We are using gitflow for the primary repository. Fork and pull request if you do not wish to use the primary repository. 

* Code review
I (SargentStudley) will be reviewing the code before accepting it. Other people may be promoted to be allowed to accept push requests if they are interested in the project enough and buy me cookies. 
* Other guidelines
-Comment your code. Don't write convoluted code - if I have to guess what it's doing, then I'm not accepting the push request!
-If you want to utilize a third party library, please ask me first. I don't want to bloat the code too much. I think we can accomplish most of what we want to do without a lot of extra libraries. 

### Who do I talk to? ###

* Contact Matthew Studley (SargentStudley on bitbucket) to discuss items about this repository. 
* Other community or team contact