from django.db import models
from django.contrib.auth.models import User

# Represents a player's ship. Ships can be part of several campaigns at once, if they want to. 
class Ship(models.Model):
    name = models.CharField(max_length=256) #The name of the ship. USN Artemis is the most obvious. 
    owner = models.ForeignKey(User) #Which user owns this ship? This is the person who can add/remove people. 
    crewMembers = models.ManyToManyField(User) #Who crews this ship? Crew members can be on several ships / campaigns if they want to be. 

#ShipTypes are used to describe the different types of ships that a crew can fly. 
#This maps directly to the ARtemis SBS ship types, and has a fixture to that effect to pre-populate the data.  
class ShipType(models.Model):
    name = models.CharField(64) #Name the ship type. 
    productionCost = models.IntegerField() #How much does it cost to build a new ship of this type? When crews die, it takes this much production to produce the ship.  

#The map model holds sectors. Each campaign uses a map. 
#Anything larger than a 10x10 is going to get a bit on the insane side I think. That's 100 sectors to make, then fight in. 
class Map(models.Model):
    name = models.CharField(max_length=256) #A simple name for the map. 
    description = models.TextField(blank=True) #A description of the map, tells a bit about it. 
    xSize = models.IntegerField() #How big across is the map? 
    ySize = models.IntegerField() #How tall / high is the map? This works just like screen resolutions folks. 

class Campaign(models.Model):
    name = models.CharField(max_length=256)
    enrolledShips = models.ManyToManyField(Ship)
    active = models.BooleanField()
    map = models.ForeignKey(Map)
    difficulty = models.IntegerField()
    dm = models.ForeignKey(User)
    turn = models.IntegerField() #Logs which turn the campaign is on. 

#A single ship can take part in several campaigns. This holds the data for a specific instance of a ship in a campaign. 
class CampaignShip(models.Model):
    ship = models.ForeignKey(Ship)
    campaign = models.ForeignKey(Campaign)
    locationX = models.IntegerField()
    locationY = models.IntegerField()
    type = models.ForeignKey(ShipType)
    destroyed = models.BooleanField(default=False)

#Sectors are the actual artemis maps that are stitched together to make one large campaign map. 
#Sectors will have a log of logic, as they manage the production of resources. Each sector has a different value. Travel can be restricted between sectors. 
class Sector(models.Model):
    map = models.ForeignKey(Map) #Which map does this sector blueprint belong to?
    locationX = models.IntegerField() #The 2D X coordinate of this sector. 
    locationY = models.IntegerField() #THe 2D Y coordinate of this sector. 
    productionPerTurn = models.IntegerField() #How much production does this sector generate per turn?
    maxProduction = models.IntegerField() #What is the maximum production that this sector can hold?
    sectionMap = models.CharField(max_length=64) #This is the encoded section map, which tells the travel logic engine if you can pass through. 
    baseFile = models.FileField(upload_to='sectors') #This is the base artemis mission file. 
    thumbNail = models.FileField(upload_to='sectorThumbs') #This is a screenshot of the map, scaled down to be smaller. 

#This is the instance of a sector in a campaign, as there can be many campaigns. 
class CampaignSector(models.Model):
    currentProduction = models.IntegerField() #How much production does this sector currently have?
    campaign = models.ForeignKey(Campaign) #What is the parent campaign for this sector?
    sector = models.ForeignKey(Sector) #What is the sector that this is an instance of?

#Missions that are Artemis Galaxy compatible generate mission logs that we can parse to figure out what happened. This model stores that information. 
#Intended use: It gets generated as the mission is handed out. 
class MissionLog(models.Model):
    campaign = models.ForeignKey(Campaign) #Which campaign was this mission for?
    sector = models.ForeignKey(CampaignSector) #Which sector was the mission in?
    ship = models.ForeignKey(CampaignShip) #Who ran the mission?
    turn = models.IntegerField() #Which turn was this mission generated on?
    generatedDate = models.DateTimeField() #When was this mission generated?
    submittedDate = models.DateTimeField(blank=True, null=True) #When was the mission log submitted? (AKA when did the players submit the mission.
    missionFile = models.TextField()
    missionLog = models.FileField(upload_to='missionLogs',blank=True, null=True)
    
    #This is an example of permissable logic in models. Computed fields are ok - actual business logic is not. 
    #Thinking here is that if there is a mission log, then the mission was run. We can use this to show a GM if missions are generated but not turned in yet. 
    def missionCompleted(self):
        if not self.missionLog:
            return True
        else:
            return False
    
    
    


    
    
    

    